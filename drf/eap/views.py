from django.shortcuts import render
from drf_renderer_xlsx.mixins import XLSXFileMixin
from drf_renderer_xlsx.renderers import XLSXRenderer
from rest_framework.decorators import api_view, action
from rest_framework.filters import SearchFilter, OrderingFilter
from rest_framework.generics import RetrieveAPIView
from rest_framework.permissions import IsAuthenticated
from rest_framework.renderers import TemplateHTMLRenderer
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.viewsets import ModelViewSet, ReadOnlyModelViewSet
from rest_framework import generics

# from .permissions import IsAuthorOrReadonly
from .serializers import EquipmentSerializer
from .models import Equipment

class EquipmentViewSet(ModelViewSet):
    queryset = Equipment.objects.all()
    serializer_class = EquipmentSerializer
    # authentication_classes = [IsAuthenticated]
    # permission_classes = [IsAuthenticated, IsAuthorOrReadonly]
    filter_backends = [SearchFilter, OrderingFilter]
    search_fields = ['eqt_tag_number']


    def perform_create(self, serializer):
        author = self.request.user
        ip = self.request.META['REMOTE_ADDR']
        serializer.save(author=author, ip=ip)

    @action(detail=False, methods=['GET'])
    def public(self,request):
        qs = self.get_queryset().filter(is_public=True)
        serializer = self.get_serializer(qs, many=True)
        return Response(serializer.data)

    @action(detail=True, methods=['PATCH'])
    def set_public(self, request, pk):
        instance = self.get_object()
        instance.is_public = True
        instance.save(update_fields=['is_public'])
        serializer = self.get_serializer(instance)
        return Response(serializer.data)

    def dispatch(self, request, *args, **kwargs):
        print("request.body :", request.body)
        print("request.POST :", request.POST)
        return super().dispatch(request, *args, **kwargs)


class EquipmentXlsxViewSet(XLSXFileMixin, ReadOnlyModelViewSet):
    queryset = Equipment.objects.all()
    serializer_class = EquipmentSerializer
    renderer_classes = (XLSXRenderer,)
    filename = 'Equipment.xlsx'


class EquipmentDetailAPIView(RetrieveAPIView):
    queryset = Equipment.objects.all()
    renderer_classes = [TemplateHTMLRenderer]
    template_name = 'instagram/equipment_detail.html'

    def get(self, request, *args, **kwargs):
        equipment = self.get_object()
        return Response({
            'equipment' : EquipmentSerializer(equipment).data,
        })