from django.urls import path, include
from rest_framework.routers import DefaultRouter
from . import views

router = DefaultRouter()
router.register('eqt', views.EquipmentViewSet)
router.register('eqt', views.EquipmentXlsxViewSet)

urlpatterns = [
    # path('public/', views.PostListAPIView.as_view()),
    # path('public/', views.public_post_list),
    path('myeqt/<int:pk>/', views.EquipmentDetailAPIView.as_view()),

    path('', include(router.urls)),
]


