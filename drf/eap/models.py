from django.conf import settings
from django.db import models

class Equipment(models.Model):
    editor = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)

    pnl_tag_number = models.CharField(max_length=30, null=True)
    substation_location = models.CharField(max_length=30, null=True)
    eqt_tag_number = models.CharField(max_length=30)
    eqt_desc = models.CharField(max_length=200)
    eqt_location = models.CharField(max_length=30)
    load_category = models.CharField(max_length=1, null=True)
    operation_duty = models.CharField(max_length=1, null=True)
    starter_type = models.CharField(max_length=10, null=True)
    eqt_type = models.CharField(max_length=10)
    bhp_absorbed_load = models.DecimalField(max_digits = 10, decimal_places = 2, null=True)
    rated_power = models.DecimalField(max_digits = 10, decimal_places = 2, null=True)
    rated_voltage = models.DecimalField(max_digits = 10, decimal_places = 2, null=True)
    rated_frequency = models.DecimalField(max_digits = 10, decimal_places = 2, null=True)
    phase_of_wires = models.DecimalField(max_digits = 5, decimal_places = 2, null=True)
    power_factor = models.DecimalField(max_digits = 5, decimal_places = 2, null=True)
    efficiency = models.DecimalField(max_digits = 5, decimal_places = 3, null=True)
    load_factor = models.DecimalField(max_digits = 5, decimal_places = 2, null=True)
    mcc_control_scheme = models.CharField(max_length=1, null=True)

    created_at = models.DateTimeField(auto_now_add=True, null=True)
    updated_at = models.DateTimeField(auto_now=True)
    is_public = models.BooleanField(default=False, db_index=True)
    ip = models.GenericIPAddressField(null=True, editable=False)

    # message = models.TextField()
    # created_at = models.DateTimeField(auto_now_add=True)
    # updated_at = models.DateTimeField(auto_now=True)
    # is_public = models.BooleanField(default=False, db_index=True)
    # ip = models.GenericIPAddressField(null=True, editable=False)
