# Generated by Django 3.1.6 on 2021-02-10 02:23

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('instagram', '0004_post_ip'),
    ]

    operations = [
        migrations.AlterField(
            model_name='post',
            name='ip',
            field=models.GenericIPAddressField(editable=False, null=True),
        ),
    ]
