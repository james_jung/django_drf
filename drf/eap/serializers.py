from django.contrib.auth import get_user_model
from rest_framework import serializers
from .models import Equipment


class AuthorSerializer(serializers.ModelSerializer):
    class Meta:
        model = get_user_model()
        fields = ['username','email']


class EquipmentSerializer(serializers.ModelSerializer):
    editor_username = serializers.ReadOnlyField(source='author.username')
    # author = AuthorSerializer()

    class Meta:
        model = Equipment
        fields = '__all__'
        # fields = [
        #     'editor_username',
        #     'eqt_tag_number',
        #     'created_at',
        #     'updated_at',
        #     'is_public',
        #     'ip',
        # ]